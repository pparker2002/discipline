// DISC inventory.js

const GAMECONST = {
    girlNames: ['Lisa', 'Lori', 'Debbie', 'Sabrina', 'Cindy', 'Evie', 'Jessica', 'Penny', 'Penelope', 'Sara', 'Jane', 'Charlotte', 'Ivy', 'Barb', 'Lily', 'Caroline','Gabi','Shelly','Reagan','Nelly', 'Emma', 'Bella', 'Eva', 'Abigail', 'Grace', 'Samantha', 'Hailee', 'Ellie', 'Autumn', 'Chloe', 'Hannah', 'Alice', 'Summer', 'Kat', 'Elena', 'Molly', 'Ariel', 'Jade', 'Anna', 'Amy', 'Elise', 'Katie'],
    hairColors: ["brown","black","chestnut","blonde","red","strawberry blonde","golden","dirty blond","auburn","brunette","dark brown","light blonde","ginger"],
    eyeColors: ["brown","blue","grey","green","hazel","dark","amber"],
    hairLengths: ['shoulder-length', 'long', 'short'],
    nipples: ['tiny', 'small, round','quarter-sized'],
    timeDilation: 1  // 1 counter = 1 minute; 1 real second * time dilation = game minutes
}

const appreciations = {
    regular: {
        level1: ["Mmm. Oh.", "Yes!", "I like it so much.", "You are very big.", "That tastes good.", "Thank you, big brother."],
        level2: ["Put it in me.", "Fuck me harder!", "Yes! Just like that!", "You're so fucking huge!", "Yes! Yes! Yes!", "Make me cum!"],
        level3: ["Fill me with your baby batter!"]
    },
    degraded: {
        level1: ["I'm sorry, master!", "I'm not worthy.", "Hurt me.", "Teach me how to please you."],
        level2: ["Split me open, master!", "Make me bleed.", "I'm your fuck puppet."],
        level3: ["I'm a slave for your cock.", "Fill this unworthy cock sleeve with your seed.","Rape me, daddy. Make it hurt.","Rape your worthless slut, daddy! Fuck her until she bleeds!"]
    },
    elevated:  {
        level1: ["Mmm. Oh.", "I love you, bro.", "Thank you for everything.", "I love your cock."],
        level2: ["Let me make you feel good.", "I'm your pretty baby.", "Yes, Daddy!"],
        level3: ["Fuck me, daddy! Yes! Yes!", "Make your little princess happy. Fill me with your baby juice.", "I love you, daddy! I'm yours forever!"]
    },
    pet:  {
        level1: ["Woof.", "Ruff."],
        level2: ["Yip yip yip."],
        level3: ["Awrooo!"]
    },
}

const apparelList = [
    {
        title: 'tshirt',
        display: 'a plain white t-shirt',
        shopCategory: 'clothes',
        cost: 10,
        dignityModifier: 0,
        appeal: 0,
        traits: [],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: true,
            isBottom: false,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: true,
        blocksVagina: false,
        blocksAnus: false
    },
    {
        title: 'wornshorts',
        display: 'faded green shorts',
        shopCategory: 'clothes',
        cost: 10,
        dignityModifier: 0,
        appeal: 0,
        traits: [],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: false,
            isBottom: true,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: true,
        blocksAnus: true
    },
    {
        title: 'cheappanties',
        display: 'cheap synthetic panties',
        shopCategory: 'clothes',
        cost: 10,
        dignityModifier: 0,
        appeal: 0,
        traits: [],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: false,
            isBottom: false,
            isUnderwear: true,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: true,
        blocksAnus: true
    },
    {
        title: 'hellokittytshirt',
        display: 'a pink Hello Kitty t-shirt',
        shopCategory: 'clothes',
        cost: 25,
        dignityModifier: 1,
        appeal: 1,
        traits: ['cute'],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: true,
            isBottom: false,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: true,
        blocksVagina: false,
        blocksAnus: false
    },
    {
        title: 'teamjersey',
        display: 'an oversized team jersey',
        shopCategory: 'clothes',
        cost: 100,
        dignityModifier: 0,
        appeal: 0,
        traits: ['sport'],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: true,
            isBottom: false,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: true,
        blocksVagina: false,
        blocksAnus: false
    },
    {
        title: 'schoolblouse',
        display: 'a white polyester school blouse',
        shopCategory: 'clothes',
        cost: 25,
        dignityModifier: 1,
        appeal: 0,
        traits: ['schoolgirl'],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: true,
            isBottom: false,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: true,
        blocksVagina: false,
        blocksAnus: false
    },
    {
        title: 'tanktop',
        display: 'a thin cotton tank top',
        shopCategory: 'clothes',
        cost: 10,
        dignityModifier: 0,
        appeal: 0,
        traits: [],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: true,
            isBottom: false,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: true,
        blocksVagina: false,
        blocksAnus: false
    },
    {
        title: 'straptanktop',
        display: 'a light pink spaghetti strap tank top',
        shopCategory: 'clothes',
        cost: 20,
        dignityModifier: 0,
        appeal: 1,
        traits: [],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: true,
            isBottom: false,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: true,
        blocksVagina: false,
        blocksAnus: false
    },
    {
        title: 'shorts',
        display: 'athletic shorts',
        shopCategory: 'clothes',
        cost: 10,
        dignityModifier: 0,
        appeal: 0,
        traits: ['sport'],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: false,
            isBottom: true,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: true,
        blocksAnus: true
    },
    {
        title: 'tornjeans',
        display: 'fashionably torn blue jeans',
        shopCategory: 'clothes',
        cost: 80,
        dignityModifier: 1,
        appeal: 0,
        traits: [],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: false,
            isBottom: true,
            isUnderwear: false,
            isLegcover: true,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: true,
        blocksAnus: true
    },
    {
        title: 'skirt',
        display: 'a knee-length tartan skirt',
        shopCategory: 'clothes',
        cost: 30,
        dignityModifier: 1,
        appeal: 1,
        traits: ['schoolgirl'],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: false,
            isBottom: true,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: false,
        blocksAnus: false
    },
    {
        title: 'miniskirt',
        display: 'a sleek black mini skirt',
        shopCategory: 'clothes',
        cost: 45,
        dignityModifier: 1,
        appeal: 1,
        traits: [],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: false,
            isBottom: true,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: false,
        blocksAnus: false
    },
    {
        title: 'denimskirt',
        display: 'a thigh-length denim skirt',
        shopCategory: 'clothes',
        cost: 35,
        dignityModifier: 0,
        appeal: 0,
        traits: [],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: false,
            isBottom: true,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: false,
        blocksAnus: false
    },
    {
        title: 'sundress',
        display: 'a cute cornflower blue sundress',
        shopCategory: 'clothes',
        cost: 50,
        dignityModifier: 1,
        appeal: 1,
        traits: [],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: true,
            isBottom: true,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: true,
        blocksVagina: false,
        blocksAnus: false
    },
    {
        title: 'cinchwaist',
        display: 'a grey plaid cinched-waist dress',
        shopCategory: 'clothes',
        cost: 35,
        dignityModifier: 1,
        appeal: 0,
        traits: [],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: true,
            isBottom: true,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: true,
        blocksVagina: false,
        blocksAnus: false
    },
    {
        title: 'panties',
        display: 'light cotton panties',
        shopCategory: 'clothes',
        cost: 5,
        dignityModifier: 0,
        appeal: 0,
        traits: [],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: false,
            isBottom: false,
            isUnderwear: true,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: true,
        blocksAnus: true
    },
    {
        title: 'flowerpanties',
        display: 'light cotton panties dotted with tiny pink flowers',
        shopCategory: 'clothes',
        cost: 5,
        dignityModifier: 0,
        appeal: 1,
        traits: ['cute'],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: false,
            isBottom: false,
            isUnderwear: true,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: true,
        blocksAnus: true
    },
    {
        title: 'boypanties',
        display: 'grey boyshort panties',
        shopCategory: 'clothes',
        cost: 5,
        dignityModifier: 0,
        appeal: 0,
        traits: ['boy'],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: false,
            isBottom: false,
            isUnderwear: true,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: true,
        blocksAnus: true
    },
    {
        title: 'lacepanties',
        display: 'black lace panties',
        shopCategory: 'clothes',
        cost: 5,
        dignityModifier: 0,
        appeal: 2,
        traits: ['sexy'],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: false,
            isBottom: false,
            isUnderwear: true,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: true,
        blocksAnus: true
    },
    {
        title: 'sneakers',
        display: 'cheap canvas sneakers',
        shopCategory: 'clothes',
        cost: 25,
        dignityModifier: 0,
        appeal: 0,
        traits: [],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: false,
            isBottom: false,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: true
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: false,
        blocksAnus: false
    },
    {
        title: 'nicesneakers',
        display: 'nice running shoes',
        shopCategory: 'clothes',
        cost: 75,
        dignityModifier: 0,
        appeal: 0,
        traits: ['sport'],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: false,
            isBottom: false,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: true
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: false,
        blocksAnus: false
    },
    {
        title: 'maryjanes',
        display: 'shiny black Mary Janes',
        shopCategory: 'clothes',
        cost: 50,
        dignityModifier: 1,
        appeal: 1,
        traits: ['schoolgirl'],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: false,
            isBottom: false,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: true
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: false,
        blocksAnus: false
    },
    {
        title: 'plainsocks',
        display: 'thin white socks',
        shopCategory: 'clothes',
        cost: 2,
        dignityModifier: 0,
        appeal: 0,
        traits: [],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: false,
            isBottom: false,
            isUnderwear: false,
            isLegcover: false,
            isSocks: true,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: false,
        blocksAnus: false
    },
    {
        title: 'athleticsocks',
        display: 'calf-length striped athletic socks',
        shopCategory: 'clothes',
        cost: 5,
        dignityModifier: 0,
        appeal: 0,
        traits: ['sport'],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: false,
            isTop: false,
            isBottom: false,
            isUnderwear: false,
            isLegcover: false,
            isSocks: true,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: false,
        blocksAnus: false
    },
    {
        title: 'dogcollar',
        display: 'a leather dog collar',
        shopCategory: 'pet',
        cost: 25,
        dignityModifier: -5,
        appeal: 1,
        traits: ['pet'],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: true,
            isTop: false,
            isBottom: false,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: false,
        blocksAnus: false
    },
    {
        title: 'catcollar',
        display: 'a fabric cat collar',
        shopCategory: 'pet',
        cost: 10,
        dignityModifier: -5,
        appeal: 1,
        traits: ['pet'],
        requirements: function() {return 1},
        location: {
            isHat: false,
            isNeckwear: true,
            isTop: false,
            isBottom: false,
            isUnderwear: false,
            isLegcover: false,
            isSocks: false,
            isShoes: false
        },
        blocksMouth: false,
        blocksBreasts: false,
        blocksVagina: false,
        blocksAnus: false
    }
]

var shopItems = [
    {
        title: 'lube',
        shopCategory: 'sex',
        itemType: 'owned',
        display: 'lube',
        verb: 'use lube',
        cost: 50,
        requirements: function() {if (player.money >= 15) {return 1} else {return 0}},
        effect: function() {yourGirl.addLube('lube'); return "You applied lube."}
    },
    {
        title: 'whip',
        shopCategory: 'sex',
        itemType: 'owned',
        display: 'leather whip',
        verb: 'use whip',
        cost: 150,
        requirements: function() {if (player.money >= 15) {return 1} else {return 0}},
        effect: function() {
            if (yourGirl.isAvailable('anus') == 0) {return 'You need to remove her clothes first.'} else {
                for (let i=0;i<5;i++) {yourGirl.abuseBody('butt')}; yourGirl.freedom -= 20; return "You lash her buttocks with the whip, leaving them red and sore."}
            }
    },
    {
        title: 'vibrator',
        shopCategory: 'sex',
        itemType: 'owned',
        display: 'vibrator',
        verb: 'use vibrator',
        cost: 100,
        requirements: function() {if (player.money >= 15) {return 1} else {return 0}},
        effect: function() {
            return "You press the vibrator against her clit."}
    },
    {
        title: 'tv',
        shopCategory: 'misc',
        itemType: 'owned',
        display: 'flatscreen tv',
        verb: 'watch tv',
        cost: 700,
        requirements: function() {if (player.money >= 15) {return 1} else {return 0}},
        effect: function() {
            return "You watch some TV."}
    },
    {
        title: 'healthymeal',
        shopCategory: 'food',
        itemType: 'consumed',
        display: 'healthy meal',
        verb: '',
        cost: 5,
        requirements: function() {if (player.money >= 15) {return 1} else {return 0}},
        effect: function() {yourGirl.weight += 400; yourGirl.hunger -= 600; yourGirl.changeDignity(1); return "She eats a healthy meal."}
    },
    {
        title: 'junkfood',
        shopCategory: 'food',
        itemType: 'consumed',
        display: 'junk food',
        verb: '',
        cost: 5,
        requirements: function() {if (player.money >= 15) {return 1} else {return 0}},
        effect: function() {yourGirl.weight += 800; yourGirl.hunger -= 600; yourGirl.changeDignity(1); return "She eats junk food."}
    }
]

var customers = [
    {
        title: "unclejohnny",
        minPay: 2,
        maxPay: 4,
        minTime: 60,
        maxTime: 80,
        requirements: function() {return 1},
        effect: function() {
            yourGirl.atHome = 0;
            postEventQueue.push([(GAMEVAR.counter+randomInt(this.minTime,this.maxTime)),'customers',customers.indexOf(this)]);
            GAMEVAR.statusQueue.push(['girlstatus',`${yourGirl.firstname} is at Uncle Johnny's house.`]);
            return `Uncle Johnny greets her as she walks up the path to his house. He smiles as his eyes trace her form, lingering lasciviously on her ${yourGirl.legs()}.`
        },
        postEffect: function() {
            yourGirl.whoredCount++;
            let payment = randomInt(this.minPay,this.maxPay)*10;
            yourGirl.atHome = 1; player.money += (payment); yourGirl.earnings += payment;
            GAMEVAR.statusQueue.push(['girlstatus',`${yourGirl.isCalled()} has returned.`]);
        }
    },
    {
        title: "mathteacher",
        minPay: 6,
        maxPay: 12,
        minTime: 80,
        maxTime: 120,
        requirements: function() {return 1},
        effect: function() {
            yourGirl.atHome = 0;
            postEventQueue.push([(GAMEVAR.counter+randomInt(this.minTime,this.maxTime)),'customers',customers.indexOf(this)]);
            GAMEVAR.statusQueue.push(['girlstatus',`${yourGirl.firstname} is at her math teacher's house.`]);
            return `You drop your sister off at her math teacher's house. He smirks as he leads her into his home.`
        },
        postEffect: function() {
            yourGirl.whoredCount++;
            let payment = randomInt(this.minPay,this.maxPay)*10;
            yourGirl.atHome = 1; player.money += (payment); yourGirl.earnings += payment;
            GAMEVAR.statusQueue.push(['girlstatus',`As you pull up to the house, the math teacher leans over to give ${yourGirl.isCalled()} a peck on the lips, his hand around her waist. ${yourGirl.isCalled()} returns to your waiting car.`]);
        }
    },
    {
        title: "subaru",
        minPay: 5,
        maxPay: 8,
        minTime: 30,
        maxTime: 80,
        requirements: function() {return 1},
        effect: function() {
            yourGirl.atHome = 0;
            postEventQueue.push([(GAMEVAR.counter+randomInt(this.minTime,this.maxTime)),'customers',customers.indexOf(this)]);
            GAMEVAR.statusQueue.push(['girlstatus',`${yourGirl.firstname} is working.`]);
            return `A middle-aged man in a 1996 Subaru pulls up to the house. He reaches across to push open the passenger door without leaving the car. ${yourGirl.isCalled()} gets in.`
        },
        postEffect: function() {
            yourGirl.whoredCount++;
            let payment = randomInt(this.minPay,this.maxPay)*10;
            yourGirl.atHome = 1; player.money += (payment); yourGirl.earnings += payment;
            GAMEVAR.statusQueue.push(['girlstatus',`The Subaru pulls back up to the house. ${yourGirl.isCalled()} walks back up the path to the house.`]);
        }
    },
    {
        title: "buddy",
        minPay: 1,
        maxPay: 5,
        minTime: 10,
        maxTime: 30,
        requirements: function() {return 1},
        effect: function() {
            yourGirl.atHome = 0;
            postEventQueue.push([(GAMEVAR.counter+randomInt(this.minTime,this.maxTime)),'customers',customers.indexOf(this)]);
            GAMEVAR.statusQueue.push(`${yourGirl.firstname} is her room with your buddy.`);
            return `Your buddy, who'd always seemed fond of ${yourGirl.isCalled()}, comes calling. He takes her by the hand and leads her back to her room. The door shuts behind them.`
        },
        postEffect: function() {
            yourGirl.whoredCount++;
            yourGirl.changeDignity(-5);
            let payment = randomInt(this.minPay,this.maxPay)*10;
            yourGirl.atHome = 1; player.money += (payment); yourGirl.earnings += payment;
            GAMEVAR.statusQueue.push(`Your buddy fist bumps you as he leaves. ${yourGirl.isCalled()} has returned.`);
        }
    }
]