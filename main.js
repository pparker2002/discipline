// DISC main.js

var inventory = []
var wardrobe = [0,1,2]
 
const navLine = `
    <a href="#" id="homebutton" onclick="showPage('main')">Home</a><a href="javascript:;" class="ml-3" id="customizebutton" onclick="showPage('customize')">Customize</a><a href="javascript:;" class="ml-3" id="statsbutton" onclick="showPage('stats')">Stats</a>
`

var GAMEVAR = {
    paused: false,
    minAge: 7,
    maxAge: 11,
    counter: 1,
    pCounter: 0,
    dayCounter: 0,
    statusQueue: [],
    navActive: 0,
    periodDuration: 15 * GAMECONST.timeDilation,
    dayDuration: 60 * 24 * GAMECONST.timeDilation,
    favoriteOutfit: [],
    currentShop: ""
}

function devPanel() {
    return (`
    <p>
        Player lust: ${player.lust}<br>
        Player arousal: ${player.arousal}<br>
        Player excitation: ${player.excitation} <br>
        Player orgasms: ${player.orgasmCount} <br>
    </p>
    <p>
        <strong>${yourGirl.isCalled()}</strong><br>
        At home: ${yourGirl.atHome}<br>
        Discipline:    ${yourGirl.disciplinePoints} <br>
        Freedom:    ${yourGirl.freedom} <br>
        Control Modifier:    ${yourGirl.controlModifier} <br>
        Dignity    ${yourGirl.dignity} <br>
        Anima:    ${yourGirl.anima} <br>
        Arousal:    ${yourGirl.arousal}<br>
        Hygiene   ${yourGirl.hygiene} <br>
        Weight:    ${yourGirl.weight} <br>
        Hunger:    ${yourGirl.hunger} <br>
        Soil (torso):    ${yourGirl.soil.torso} <br>
        Soil (mouth):    ${yourGirl.soil.mouth}   <br>
        Soil (vagina):   ${yourGirl.soil.vagina}  <br>
        Soil (anus):    ${yourGirl.soil.anus}<br>
        Trauma (butt):   ${yourGirl.trauma.butt}<br>
        Trauma (vagina):  ${yourGirl.trauma.vagina}<br>
        Truma (anus): ${yourGirl.trauma.anus}<br>
        Lube: ${yourGirl.lubrication} <br>
        Lubesource:    ${yourGirl.lubeSource} <br>
        Fellatio Skill:    ${yourGirl.fellatioSkill} <br>
        Verbal Skill: ${yourGirl.verbalSkill}<br>
        Kiss Skill: ${yourGirl.kissSkill}<br>
        V Skill: ${yourGirl.vaginalSkill} <br>
        A Skill:   ${yourGirl.analSkill} <br>
        F Count:   ${yourGirl.fellatioCount} <br>
        V Count:   ${yourGirl.vaginalCount} <br>
        A Count:   ${yourGirl.analCount} <br>
        Orgasms:   ${yourGirl.orgasmCount} <br>
        Whored:  ${yourGirl.whoredCount}<br>
        Earnings:    ${Math.trunc(yourGirl.earnings)} <br>
    </p>
    `)
}

function statsPage() {
    let stats = '';
    stats += `
    <div class="col mt-3">
            <h3>Stats</h3>
            <p>
    `
    if (yourGirl.fellatioCount > 0) {
        stats += `
        Fellatio count: ${yourGirl.fellatioCount}<br>
        Vaginal sex count: ${yourGirl.vaginalCount}<br>
        `
    }
    if (yourGirl.whoredCount > 0) {
        stats += `
        Total earnings: ${yourGirl.earnings}<br>
        Average earnings: ${Math.trunc(yourGirl.averageEarnings())}<br>
        `
    }
    stats += `
            </p>
            <p>
            <button class="btn btn-danger" onclick="resetGame()">reset game</button>
            </p>
        </div>
    `
    return stats
}

function customizePage() {
    return (`
        <div class="col-md mt-3">
            <h3>Customize ${yourGirl.firstname}</h3>
            <div class="mt-3" id="vdescribe">${yourGirl.verboseDescription()}</div>
            <div class="mt-3" id="customstatus"></div>
            <div class="mt-3" id="customcontrols"></div>
            <div class="mt-3" id="inventorysection"></div>
        </div>
        <div class="col-md mt-3" id="shoppingcolumn">
            <h3 class="mt-3">Shopping</h3>
            <div class="mt-3">$<span id="funds"></span></div>
            <div class="mt-3" id="shopstatus"></div>
            <div class="mt-3" id="shopcontrols"></div>
            <div class="mt-3" id="shop"></div>
        </div>
    `)
}

function mainPage() {
    return (`
        <div class="col-md-8">
          <h1 class="mt-3" id="titletext">${yourGirl.isCalled()}.</h1>
          <div class="mt-3" id="playerstatus"></div>
          <div class="row">
            <div class="col" id="primarytext">
                <div class="mt-3" id="physical"></div>
                <div class="mt-3" id="clothing"></div>
            </div>
            
          </div>
          <div class="mt-3" id="statustext">${yourGirl.currentStatus}</div>
          <div class="row mt-2" id="controlrow">
          <div class="col-sm"><div id="controls"></div></div>
          <div class="col-sm"><div id="traincontrols"></div></div>
          <div class="col-sm"><div id="thirdcontrols"></div></div>
          </div>
          <div class="mt-3" id="feedback"></div>
        </div>
        <div class="col-md-4 mt-5" id="dev"></div>
    `)
}

function Player() {
    this.lust = 0,
    this.arousal = 0,
    this.excitation = 0,
    this.orgasmThreshold = 15,
    this.orgasmCount = 0,
    this.money = 500
}

Player.prototype.addLust = function() {
    let mod = 0;
    if (yourGirl.atHome == 1) {
        mod += yourGirl.calcAppeal();
    }
    this.lust += mod;
}

Player.prototype.addArousal = function(type) {
    let mod = 0;
    if (yourGirl.atHome == 1) {
        mod += yourGirl.calcAppeal();
    }
    if (type == 'time') {
        mod -= 1;
    } else if (type == 'discipline') {
        mod += 2;
    } else if (type == 'nudity') {
        mod += 1;
    } else if (type == 'direct') {
        mod += 3
    }
    this.arousal += mod;
}

Player.prototype.addExcitation = function() {
    if (this.excitation > this.orgasmThreshold) {
        this.lust = 0;
        this.arousal = 0;
        this.excitation = 0;
        this.orgasmCount ++;
        return 1
    } else {
        this.excitation ++;
        return 0
    }
}

Player.prototype.reduceExcitation = function() {
    if (this.excitation > 0) {
        this.excitation--;
    }
}

Player.prototype.reduceArousal = function() {
    if (this.arousal > 0) {
        this.arousal--;
    }
}

Player.prototype.getStatus = function() {
    let st = "";
    if (this.lust >= 1000) {
        st+="You are sexually frustrated. "
    } else if (this.lust >= 500) {
        st+="You are very horny. "
    } else if (this.lust >= 300) {
        st+="You are a little horny. "
    }
    if (this.arousal >= 100) {
        st+="You are rock-hard. "
    } else if (this.arousal >= 50) {
        st+="You are aroused. "
    } else if (this.arousal >= 30) {
        st+="You are somewhat aroused. "
    }
    st += `You have $${this.money}.`
    return st;
}

Player.prototype.processTick = function() {
    player.reduceExcitation();
    player.reduceArousal();
}

Player.prototype.processPeriodic = function() {
    player.addLust();
}

var player = new Player;
var postEventQueue = [];
var gameTimer = new Date();

function addAction(id) {
    let buttonclass = "btn-secondary";
    if (gameActions[id].type == 'custom') {
        buttonclass = "btn-info"
    } else if (gameActions[id].type == 'shop') {
        buttonclass = 'btn-warning'
    } else if (gameActions[id].type == 'training') {
        buttonclass = 'btn-primary'
    }
    let button = `<p class="mt-3"><button class="btn ${buttonclass}" id="${gameActions[id].title}" onclick="performAction(${id})">${gameActions[id].display}</button></p>`
    if (gameActions[id].type == 'default') {
        $("#controls").append(button)
    } else if (gameActions[id].type == 'training') {
        $("#traincontrols").append(button)
    } else if (gameActions[id].type == 'custom') {
        $("#customcontrols").append(button)
    } else if (gameActions[id].type == 'shop') {
        $("#shopcontrols").append(button)
    }
}

function removeAction(id) {
  $(`#${gameActions[id].title}`).remove()
}

function performAction(id) {
    closeExpansions();
    let divid = "#feedback";
    if (gameActions[id].type == 'custom') {divid = "#customstatus"}
    if (gameActions[id].type == 'shop') {divid = "#shopstatus"}
    $(divid).hide();
    $(divid).html(gameActions[id].effect());
    $(divid).fadeIn(100);
    saveGame();
}

function Girl() {
  this.age = randomInt(GAMEVAR.minAge,GAMEVAR.maxAge);
  this.firstname = pickFrom(GAMECONST.girlNames);
  this.nickname;
  this.hairColor = pickFrom(GAMECONST.hairColors);
  this.eyeColor = pickFrom(GAMECONST.eyeColors);
  this.hairLength = pickFrom(GAMECONST.hairLengths);
  this.nipples = pickFrom(GAMECONST.nipples);
  this.equippedClothes = [0,1,2];
  this.atHome = 1;
  this.disciplinePoints = 0;
  this.freedom = 100;
  this.controlModifier = 5;
  this.dignity = 100;
  this.anima = 0;
  this.arousal = 0;
  this.hygiene = 3;
  this.weight = 150000;
  this.hunger = 0;
  this.soil = {
      torso: 0,
      mouth: 0,
      vagina: 0,
      anus: 0
  };
  this.trauma = {
      butt: 0,
      vagina: 0,
      anus: 0
  }
  this.lubrication = 0;
  this.lubeSource = "";
  this.currentStatus = `${this.firstname} gets on your nerves.`;
  this.verbalSkill = 0;
  this.kissSkill = 0;
  this.fellatioSkill = 0;
  this.vaginalSkill = 0;
  this.analSkill = 0;
  this.fellatioCount = 0;
  this.vaginalCount = 0;
  this.analCount = 0;
  this.orgasmCount = 0;
  this.whoredCount = 0;
  this.earnings = 0;
  this.vv = 1;
  this.av = 1
}

Girl.prototype.averageEarnings = function() {
    return (this.earnings/this.whoredCount)
}

Girl.prototype.isCalled = function() {
    if (this.nickname != undefined) {
        return this.nickname
    } else {
        return this.firstname
    }
}

Girl.prototype.calcFreedom = function() {
    if (this.freedom < 100 && randomInt(1,this.controlModifier) == 1) {
        this.freedom += this.age;
        //console.log("increasing freedom " + this.freedom)
    }
}

Girl.prototype.getStatus = function() {
    let status = "";
    let highfreedom = [
        ` is running around the house recklessly. `
    ]
    let medfreedom = [
        ` is blabbering on about something. `,
        ` is getting on your nerves. `
    ]
    let lowfreedom = [
        ` is hanging out peacefully. `
    ]
    if (this.freedom >= 100) {
        status += (this.isCalled() + pickFrom(highfreedom))
    } else if (this.freedom >= 50) {
        status += (this.isCalled() + pickFrom(medfreedom))
    } else {
        status += (this.isCalled() + pickFrom(lowfreedom))
    }
    if (this.hunger > 500) {
        status += "She is hungry. "
    }
    return status
}

Girl.prototype.discipline = function() {
    let startdiscipline = this.freedom + 0;
    this.freedom -= this.controlModifier;
    if (startdiscipline >= 0 && this.freedom < 0) {
        this.controlModifier += 1;
    }
    // console.log("reducing freedom");
}

Girl.prototype.changeDignity = function(num) {
    if (num > 0 && this.dignity < 1000) {
        this.dignity += num;
    } else if (num < 0 && this.dignity > -1000) {
        this.dignity += num;
    }
}

Girl.prototype.addArousal = function() {
    this.arousal ++;
    if (this.arousal > 5) {
        this.addLube('natural');
    }
}

Girl.prototype.addLube = function(source) {
    if (this.lubrication <= 10) {
        this.lubrication += 10;
    }
    this.lubeSource = source;
}

Girl.prototype.removeLube = function() {
    let resultText = "";
    if (yourGirl.lubrication == 0) {
        this.lubeSource = "";
        resultText += "It is dry and painful. She winces in discomfort. "
    } else if (yourGirl.lubeSource == 'semen') {
        resultText += "Wet semen coats her slit. "
    } else if (yourGirl.lubeSource == 'saliva') {
        resultText += "Your saliva lubricates entrance. "
    } else if (yourGirl.lubeSource == 'natural') {
        resultText += "Her body's natural lubrication welcomes you in. "
    }
    if (this.lubrication > 0) {
        this.lubrication--;
    }
    return resultText;
}

Girl.prototype.abuseBody = function(part) {
    this.trauma[part]++;
}

Girl.prototype.breasts = function() {
    if (this.age >= 10) {
        return "budding breasts"
    } else {
        return "flat, boyish chest"
    }
}

Girl.prototype.legs = function() {
    let legcovered = false;
    for (let i=0;i<this.equippedClothes.length;i++) {
        if (apparelList[wardrobe[this.equippedClothes[i]]].location.isLegcover == true) {
            legcovered = true;
        }
    }
    if (legcovered == false) {
        return 'bare legs'
    } else {
        return 'youthful legs'
    }
}

Girl.prototype.wearApparel = function(wardrobeindex) {
    let worn = [];
    for (let i=0;i<this.equippedClothes.length;i++) {
        for (var property in apparelList[wardrobe[this.equippedClothes[i]]].location) {
            if (apparelList[wardrobe[this.equippedClothes[i]]].location[property] == true && apparelList[wardrobe[wardrobeindex]].location[property] == true) {
                console.log("already wearing " + property);
                worn.push(this.equippedClothes[i]);
            }
        }
    }
    // console.log(worn);
    if (worn.length > 0) {
        for (let i=0;i<worn.length;i++) {
            console.log("removing " + this.equippedClothes.splice(this.equippedClothes.indexOf(worn[i]),1));
        }
    }
    this.equippedClothes.push(wardrobeindex);
    return this.currentOutfit();
}

Girl.prototype.removeApparel = function(index) {
    console.log("removing " + this.equippedClothes.splice(index,1));
    return this.currentOutfit();
}

Girl.prototype.currentOutfit = function() {
    let desc = "";
    let underwear = 0;
    if (this.equippedClothes.length == 0) {
        desc += `${this.isCalled()} is naked.`;
    } else if (this.equippedClothes.length == 1) {
        desc += `${this.isCalled()} is wearing ${apparelList[wardrobe[this.equippedClothes[0]]].display}. `;
        if (apparelList[wardrobe[this.equippedClothes[0]]].location.isUnderwear == true) {underwear = 1}
        if (underwear == 0) {desc += "She is not wearing any underwear. "}
    } else {
        desc += `${this.isCalled()} is wearing `;
        for (let i=0;i<this.equippedClothes.length;i++) {
            if (apparelList[wardrobe[this.equippedClothes[i]]].location.isUnderwear == true) {underwear = 1}
            if (i == this.equippedClothes.length-1) {
                desc += `and ${apparelList[wardrobe[this.equippedClothes[i]]].display}`;
            } else {
                desc += (apparelList[wardrobe[this.equippedClothes[i]]].display + ", ");
            }
        }
        desc += ". "
        if (underwear == 0) {desc += "She is not wearing any underwear. "}
    }
    return desc
}

Girl.prototype.isDressed = function() {
    let hastop = 0;
    let hasbottom = 0;
    for (let i=0;i<this.equippedClothes.length;i++) {
        if (apparelList[wardrobe[this.equippedClothes[i]]].location.isBottom == true) {
            hasbottom ++;
        }
        if (apparelList[wardrobe[this.equippedClothes[i]]].location.isTop == true) {
            hastop ++;
        }
    }
    if (hastop == 1 && hasbottom == 1) {
        return 1
    } else {
        return 0
    }
}

Girl.prototype.isNude = function() {
    for (let i=0;i<this.equippedClothes.length;i++) {
        if (apparelList[wardrobe[this.equippedClothes[i]]].location.isBottom == true ||
            apparelList[wardrobe[this.equippedClothes[i]]].location.isTop == true ||
            apparelList[wardrobe[this.equippedClothes[i]]].location.isUnderwear == true ) {
            return 0;
        }
    }
    return 1
}

Girl.prototype.isAvailable = function(part) {
    if (part == 'mouth') {
        for (let i=0;i<this.equippedClothes.length;i++) {
            if (apparelList[wardrobe[this.equippedClothes[i]]].blocksMouth == true) {
                return 0
            }
        }
        return 1
    }
    if (part == 'vagina') {
        for (let i=0;i<this.equippedClothes.length;i++) {
            if (apparelList[wardrobe[this.equippedClothes[i]]].blocksVagina == true) {
                return 0
            }
        }
        return 1
    }
    if (part == 'anus') {
        for (let i=0;i<this.equippedClothes.length;i++) {
            if (apparelList[wardrobe[this.equippedClothes[i]]].blocksAnus == true) {
                return 0
            }
        }
        return 1
    }
    if (part == 'breasts') {
        for (let i=0;i<this.equippedClothes.length;i++) {
            if (apparelList[wardrobe[this.equippedClothes[i]]].blocksBreasts == true) {
                return 0
            }
        }
        return 1
    }
}

Girl.prototype.calcAppeal = function() {
    let appeal = 0;
    if (this.isNude() == 1) {
        appeal += 10;
    } else {
        for (let i=0;i<this.equippedClothes.length;i++) {
            appeal += apparelList[wardrobe[this.equippedClothes[i]]].appeal;
        }
    }
    if (this.dignity < -100) {
        for (var property in this.soil) {
                appeal += this.soil[property];
        }
    }
    return appeal
}

Girl.prototype.terseDescription = function() {
    return (
        `You recently moved back home after your parents abruptly passed away, leaving you to take care of your ${this.age}-year-old sister, ${this.firstname}. ${this.isCalled()} has ${this.hairLength} ${this.hairColor} hair and pretty ${this.eyeColor} eyes.`
    )
}

Girl.prototype.verboseDescription = function() {
    let description = "<p>";
    description += (`${this.firstname} has ${this.hairLength} ${this.hairColor} hair and pretty ${this.eyeColor} eyes. `);
    if (this.weight > 140000) {
        description += `She is an average weight. `
    } else if (this.weight > 120000) {
        description += `She is thin. `
    } else if (this.weight > 90000) {
        description += 'She is skinny. '
    } else if (this.weight > 30000) {
        description += 'She is emaciated '
    } else {
        description += 'She is dangerously underweight. '
    }
    description += `${this.currentOutfit()} `;
    if (this.isAvailable('breasts') == 1) {
        description += `${upCase(this.nipples)} nipples adorn her ${this.breasts()}. `
    }
    if (this.isAvailable('vagina') == 1) {
        description += `Her tiny pussy is hairless and smooth. `
    }
    description += "</p><p>"
    if (this.trauma.butt > 50) {
        description += "Her butt is scarlet red, her delicate skin raw and scarred. "
    } else if (this.trauma.butt > 10) {
        description += "Her ass cheeks are pink and sore. "
    }
    if (this.trauma.vagina > 0) {
        description += "Her labia are swollen. "
    }
    if (this.trauma.anus > 0) {
        description += "Her asshole is distended and bruised. "
    }
    description += "</p><p>"
    if (this.hygiene < -5) {
        description += "She is filthy. It's hard to know when she last bathed, but her body smells like the sewer below a brothel, her skin mottled with dried remnants of sweat, grime, and seminal fluid. "
    }
    if (this.soil.torso > 0) {
        description += "Her body is covered with ejaculate. "
    }
    if (this.soil.mouth > 0) {
        description += "Dried semen coats her lips and chin. "
    }
    if (this.soil.vagina > 0) {
        description += "Your cum drips from her vagina. "
    }
    if (this.soil.anus > 0) {
        description += "Your sperm is smeared across her buttocks. "
    }
    description += "</p>"
    return description
}

Girl.prototype.calcDignity = function() {
    let mod = 1;
    let ani = 0;
    if (this.isNude() == 1) {mod -= 5}
    for (let i=0;i<this.equippedClothes.length;i++) {
        mod += apparelList[wardrobe[this.equippedClothes[i]]].dignityModifier;
        for (let j=0;j<apparelList[wardrobe[this.equippedClothes[i]]].traits.length;j++) {
            if (apparelList[wardrobe[this.equippedClothes[i]]].traits[j] == 'pet' && this.isNude() == 1) {ani ++;}
        }
    }
    mod += this.hygiene;
    for (property in this.soil) {
        mod -= this.soil[property];
    }
    this.changeDignity(mod);
    this.anima += ani;
}

Girl.prototype.processTick = function() {
    this.calcFreedom();
    this.calcDignity();
    this.weight --;
    this.hunger ++;
}

Girl.prototype.processPeriodic = function() {
    this.removeLube();
    for (property in this.trauma) {
        if (this.trauma[property] > 0) {
            this.trauma[property] --
        }
    }
}

Girl.prototype.processDay = function() {
    this.hygiene--;
}

function pickFrom(array) {
    var choice = Math.floor(Math.random() * array.length);
    return array[choice];
}

function findDiff(a, b) {
    return Math.abs(a-b);
}

function randomInt(min, max) {
    var a = Math.floor(Math.random() * (max + 1 - min)) + min;
    return a;
}

function upCase(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

var yourGirl = new Girl;

function gameStart() {
    gameSetup();
    gameStarter();
}

function gameSetup() {
    loadGame();
    if (localStorage.getItem('discSaveGame') != 1) {
        randomOutfit();
    }
    showPage('main');
}

function gameStarter() {
    setTimeout(function() {
        if (GAMEVAR.paused == false) {
            let nowTime = new Date();
            let elapsedTime = Math.floor((nowTime.getTime() - gameTimer.getTime())/1000);
            let elapsedPeriods = Math.floor(elapsedTime/GAMEVAR.periodDuration);
            let elapsedDays = Math.floor(elapsedTime/GAMEVAR.dayDuration);
            if (elapsedTime > GAMEVAR.counter) {
                for (let i=0;i<(elapsedTime-GAMEVAR.counter);i++) {
                    gameLoop();
                }
            }
            if (elapsedPeriods > GAMEVAR.pCounter) {
                let cycles = (elapsedPeriods - GAMEVAR.pCounter);
                for (let i=0;i<cycles;i++) {
                    periodicLoop();
                }
            }
            if (elapsedDays > GAMEVAR.dayCounter) {
                let cycles = (elapsedDays - GAMEVAR.dayCounter);
                for (let i=0;i<cycles;i++) {
                    dayLoop();
                }
            }
        }
        displayLoop();
        gameStarter();
    }, 20);
}

function gameLoop() {
    yourGirl.processTick();
    player.processTick();
    GAMEVAR.counter++;
    // console.log("tick");
}

function periodicLoop() {
    if (yourGirl.getStatus() != $("#statustext").html() && yourGirl.atHome == 1) {
        GAMEVAR.statusQueue.push(['girlstatus', yourGirl.getStatus()]);
    }
    yourGirl.processPeriodic();
    player.processPeriodic();
    GAMEVAR.pCounter++;
    // console.log("periodic");
}

function dayLoop() {
    GAMEVAR.dayCounter++;
    yourGirl.processDay();
    console.log("daily");
}

function displayLoop() {
    calcStatus();
}

var dataBindings = [
    [ "#playerstatus" , function() {return player.getStatus()} ],
    [ "#vdescribe" , function() {return yourGirl.verboseDescription()} ],
    [ "#clothing", function() {return yourGirl.currentOutfit()}],
    [ "#physical", function() {return yourGirl.terseDescription()}],
    [ "#funds", function() {return player.money}],
    [ "#dev", function() {return devPanel()}]
]

function calcStatus() {
    if (GAMEVAR.statusQueue.length > 0) {
        let targetdiv = ""
        while (GAMEVAR.statusQueue.length > 0) {
            let newstatus = GAMEVAR.statusQueue.shift();
            switch (newstatus[0]) {
                case 'girlstatus':
                    targetdiv = "#statustext";
                    yourGirl.currentStatus = newstatus[1];
                    break;
                case 'shopstatus':
                    targetdiv = "#shopstatus";
                    break;
                case 'customstatus':
                    targetdiv = "#customstatus";
                    break;
                default:
                    break;
            }
            $(targetdiv).hide();
            $(targetdiv).html(newstatus[1]);
            $(targetdiv).fadeIn(200);
        }
    }
    for (let i=0;i<gameActions.length;i++) {
        if (gameActions[i].requirements() == 1 && gameActions[i].active == 0) {
        addAction(i);
        gameActions[i].active = 1;
        }
    }
    for (let i=0;i<gameActions.length;i++) {
        if (gameActions[i].requirements() == 0 && gameActions[i].active == 1) {
        removeAction(i);
        gameActions[i].active = 0;
        }
    }
    for (let j=0;j<postEventQueue.length;j++) {
        if (postEventQueue[j][0] <= GAMEVAR.counter) {
            if (postEventQueue[j][1] == 'customers') {
                customers[postEventQueue[j][2]].postEffect();
            } else if (postEventQueue[j][1] == 'gameActions') {
                gameActions[postEventQueue[j][2]].postEffect();
            }
            postEventQueue.splice(j);
        }
    }
    for (let k=0;k<dataBindings.length;k++) {
        if (dataBindings[k][1]() != $(dataBindings[k][0]).html()) {
            $(dataBindings[k][0]).html(dataBindings[k][1]());
        }
    }
}

function saveGame() {
    localStorage.setItem('discSaveGame', 1);
    localStorage.setItem('gamevar', JSON.stringify(GAMEVAR));
    localStorage.setItem('wardrobe', JSON.stringify(wardrobe));
    localStorage.setItem('inventory', JSON.stringify(inventory));
    localStorage.setItem('player', JSON.stringify(player));
    localStorage.setItem('yourgirl', JSON.stringify(yourGirl));
    localStorage.setItem('postEventQueue', JSON.stringify(postEventQueue));
    localStorage.setItem('gametimer', gameTimer.getTime());
}

function loadGame() {
    if (localStorage.getItem('discSaveGame') == 1) {
        GAMEVAR = JSON.parse(localStorage.getItem('gamevar'));
        wardrobe = JSON.parse(localStorage.getItem('wardrobe'));
        inventory = JSON.parse(localStorage.getItem('inventory'));
        postEventQueue = JSON.parse(localStorage.getItem('postEventQueue'));
        gameTimer = new Date(parseInt(localStorage.getItem('gametimer')));
        var yourgirlcopy = JSON.parse(localStorage.getItem('yourgirl'));
        for (var property in yourgirlcopy) {
            if (yourgirlcopy.hasOwnProperty(property)) {
                yourGirl[property] = yourgirlcopy[property];
            }
        }
        var playercopy = JSON.parse(localStorage.getItem('player'));
        for (var property in playercopy) {
            if (playercopy.hasOwnProperty(property)) {
                player[property] = playercopy[property];
            }
        }
    }
    if (GAMEVAR.navActive == 1) {
        $("#navline").html(navLine);
    }
}

function resetGame() {
    localStorage.clear();
    location.reload();
}

var gameActions = [
    {
        title: 'discipline',
        type: 'default',
        display: 'Discipline',
        active: 0,
        requirements: function() {if (yourGirl.disciplinePoints < 10 && yourGirl.atHome == 1) {return 1} else {return 0}},
        effect: function() {
            yourGirl.discipline();
            if (yourGirl.isDressed() == 0) {
                yourGirl.changeDignity(-1);
                player.addArousal('discipline');
                player.addArousal('nudity');
                yourGirl.abuseBody('butt');
            } else {
                player.addArousal('discipline');
            }
            let resultText = [
                'You spank her small bottom.',
                'Your open palm thwacks against her pert behind.'
            ]
            return pickFrom(resultText)
        },
        postEffect: function() {}
    },
    {
        title: 'finger',
        type: 'default',
        display: 'Finger her',
        active: 0,
        requirements: function() {if (yourGirl.atHome == 1 && yourGirl.isAvailable('vagina') == 1 && player.arousal > 100) {return 1} else {return 0}},
        effect: function() {
            let resultText = [
                'You slide your finger in and out her pussy.'
            ]
            if (yourGirl.freedom > 0) {
                yourGirl.addArousal();
            } else {
                yourGirl.changeDignity(-1);
            }
            return pickFrom(resultText)
        },
        postEffect: function() {}
    },
    {
        title: 'cunnilingus',
        type: 'default',
        display: 'Perform cunnilingus',
        active: 0,
        requirements: function() {if (yourGirl.atHome == 1  && yourGirl.isAvailable('vagina') == 1 && player.arousal > 100) {return 1} else {return 0}},
        effect: function() {
            yourGirl.addLube('saliva');
            if (yourGirl.freedom > 0) {
                yourGirl.addArousal();
            }
            yourGirl.changeDignity(1);
            let resultText = [
                'Your tongue descends on her labia.',
                'You push your tongue deep into her snatch.'
            ]
            return pickFrom(resultText)
        },
        postEffect: function() {}
    },
    {
        title: 'disrobe',
        type: 'default',
        display: 'Strip her',
        active: 0,
        requirements: function() {if (yourGirl.controlModifier > 4 && yourGirl.isNude() == 0 && yourGirl.atHome == 1) {return 1} else {return 0}},
        effect: function() {
            GAMEVAR.navActive = 1; $("#navline").html(navLine); 
            $("#clothing").html("She is naked."); 
            yourGirl.equippedClothes = [];
            return 'You pull her clothes off, leaving her naked.'},
        postEffect: function() {}
    },
    {
        title: 'removeallclothes',
        type: 'custom',
        display: 'Remove all clothes',
        active: 0,
        requirements: function() {if (yourGirl.controlModifier > 4 && yourGirl.isNude() == 0 && yourGirl.atHome == 1) {return 1} else {return 0}},
        effect: function() {
            GAMEVAR.navActive = 1; $("#navline").html(navLine); 
            $("#clothing").html("She is naked."); 
            yourGirl.equippedClothes = [];
            return 'You pull her clothes off, leaving her naked.'},
        postEffect: function() {}
    },
    {
        title: 'forcefellatio',
        type: 'default',
        display: 'Force fellatio',
        active: 0,
        requirements: function() {if ((player.arousal > 100 || yourGirl.fellatioCount > 0) && yourGirl.atHome == 1 && yourGirl.isAvailable('mouth') == 1) {return 1} else {return 0}},
        effect: function() {
            player.addArousal('direct');
            let resultText = [
                'You pull her mouth down over your erect cock.',
                'Your hands grasp her head, forcing your cock to the back of her throat. She gags.'
            ]
            let orgasmText = [
                'You cum into her throat before withdrawing. She looks up at you, semen dripping from the sides of her mouth.'
            ]
            yourGirl.changeDignity(-1);
            let oResult = player.addExcitation();
            if (oResult == 1) {
                yourGirl.fellatioCount++;
                yourGirl.soil.mouth++;
                return pickFrom(orgasmText);
            } else if (player.arousal < 100) {
                return "You force your flaccid cock into her mouth, rubbing it slowly against her lips and tongue. "
            } else {
                return pickFrom(resultText)
            }
        },
        postEffect: function() {}
    },
    {
        title: 'appreciations',
        type: 'training',
        display: 'Teach her appreciation',
        active: 0,
        requirements: function() {if (yourGirl.atHome == 1 && yourGirl.freedom < 10) {return 1} else {return 0}},
        effect: function() {
            let resultText = "";
            if (yourGirl.dignity < -200) {
                if (yourGirl.verbalSkill > 100) {
                    resultText = pickFrom(appreciations.degraded.level3);
                } else if (yourGirl.verbalSkill > 20) {
                    resultText = pickFrom(appreciations.degraded.level2);
                } else {
                    resultText = pickFrom(appreciations.degraded.level1);
                }
            } else if (yourGirl.dignity > 200) {
                if (yourGirl.verbalSkill > 100) {
                    resultText = pickFrom(appreciations.elevated.level3);
                } else if (yourGirl.verbalSkill > 20) {
                    resultText = pickFrom(appreciations.elevated.level2);
                } else {
                    resultText = pickFrom(appreciations.elevated.level1);
                }
            } else {
                if (yourGirl.verbalSkill > 100) {
                    resultText = pickFrom(appreciations.regular.level3);
                } else if (yourGirl.verbalSkill > 20) {
                    resultText = pickFrom(appreciations.regular.level2);
                } else {
                    resultText = pickFrom(appreciations.regular.level1);
                }
            }
            yourGirl.verbalSkill++;
            return (`"${resultText}"`)
        },
        postEffect: function() {}
    },
    {
        title: 'kiss',
        type: 'training',
        display: 'Kiss her',
        active: 0,
        requirements: function() {if (player.lust > 20 && yourGirl.atHome == 1 && yourGirl.isAvailable('mouth') == 1) {return 1} else {return 0}},
        effect: function() {
            let resultText = [
                'You press your lips to hers. She makes no response.',
                'You force your tongue past her teeth to taste the inside of her mouth. Her tongue is motionless, her body stiff.'
            ]
            yourGirl.changeDignity(1);
            if ((yourGirl.dignity > 0 && yourGirl.freedom > -10) || (yourGirl.dignity < -200 && yourGirl.freedom < 0)) {
                yourGirl.kissSkill++;
            }
            return pickFrom(resultText)
        },
        postEffect: function() {}
    },
    {
        title: 'trainfellatio',
        type: 'training',
        display: 'Train fellatio',
        active: 0,
        requirements: function() {if (player.arousal > 100 && yourGirl.atHome == 1 && yourGirl.isAvailable('mouth') == 1 && yourGirl.freedom < 10) {return 1} else {return 0}},
        effect: function() {
            let resultText = [
                'You pull her mouth down over your erect cock.',
                'Your hands grasp her head, forcing your cock to the back of her throat. She gags.'
            ]
            let orgasmText = [
                'You cum into her throat before withdrawing. She looks up at you, semen dripping from the sides of her mouth.'
            ]
            let oResult = player.addExcitation();
            if (oResult == 1) {
                yourGirl.fellatioSkill++;
                yourGirl.fellatioCount++;
                yourGirl.soil.mouth++;
                return pickFrom(orgasmText);
            } else {
                return pickFrom(resultText)
            }
        },
        postEffect: function() {}
    },
    {
        title: 'forcevaginal',
        type: 'default',
        display: 'Force sex',
        active: 0,
        requirements: function() {if (player.arousal > 150 && yourGirl.atHome == 1 && yourGirl.isAvailable('vagina') == 1) {return 1} else {return 0}},
        effect: function() {
            yourGirl.abuseBody('vagina');
            let resultText = "";
            let virginityText = [
                'You force your cock deep into her pussy, tearing her hymen and permanently altering your relationship with one another. A trail of blood follows your penis as it withdraws. Her face displays a mix of shock and fear, tears streaking down her cheek. A soft whimpering escapes her throat. '
            ]
            let regularText = [
                'You push her back on the couch and force your penis into her tight pussy. ',
                'Your cock stretches her tight cunt. '
            ]
            let orgasmText = [
                'You ejaculate into her unprotected womb, a trail of viscous semen follows your cock out as you withdraw. '
            ]
            let verbalText = [
                `"Fuck your pathetic slave," she moans. "Make it hurt." `,
                `"It hurts so much, big brother." She looks pleadingly into your eyes. "Give me more." `,
                `"Fuck me, daddy! Fill your worthless little slut with cum!"`,
                `"My tight little ${yourGirl.age}-year-old pussy belongs to you!"`
            ]
            yourGirl.changeDignity(-1);
            if (yourGirl.vv == 1) {
                resultText += pickFrom(virginityText);
                yourGirl.vv = 0;
                yourGirl.changeDignity(-100);
                return resultText;
            } else {
                resultText += pickFrom(regularText);
            }
            if (yourGirl.lubrication == 0) {
                yourGirl.changeDignity(-1);
            }
            resultText += yourGirl.removeLube();
            if (yourGirl.dignity < -100 && yourGirl.verbalSkill > 50) {
                resultText += pickFrom(verbalText);
            }
            let oResult = player.addExcitation();
            if (oResult == 1) {
                yourGirl.vaginalCount++;
                yourGirl.addLube('semen');
                yourGirl.soil.vagina++;
                return pickFrom(orgasmText);
            } else {
                return resultText
            }
        },
        postEffect: function() {}
    },
    {
        title: 'forceanal',
        type: 'default',
        display: 'Force anal',
        active: 0,
        requirements: function() {if (player.arousal > 150 && yourGirl.atHome == 1 && yourGirl.isAvailable('anus') == 1) {return 1} else {return 0}},
        effect: function() {
            let lubeindex = -1;
            let haslube = 0;
            for (let i=0;i<shopItems.length;i++) {
                if (shopItems[i].title == 'lube') {
                    lubeindex = i;
                }
            }
            for (let i=0;i<inventory.length;i++) {
                if (inventory[i] == lubeindex) {
                    haslube = 1;
                }
            }
            if (haslube == 0 && yourGirl.analCount < 10) {return 'Her asshole is far too tight to penetrate without lubrication. '} else {
                yourGirl.abuseBody('anus');
                let resultText = "";
                let virginityText = [
                    'After applying a generous amount of lube, you slowly push your rock-hard cock millimeter by millimeter into her extraordinarily tight sphincter. No amount of discipline can restrain her from crying out in pain from the violation. Once fully encased, you rest your chest against her back, enjoying the warmth and pressure against your cock. You hear her sobbing quietly into her pillow. '
                ]
                let regularText = [
                    'You bend her over, violating her anus with your erection. ',
                    'She emits pained yelps as you saw in and out of her tight anus. '
                ]
                let orgasmText = [
                    'You ejaculate into her darkest depths. After you pull out, her asshole ejects globules of semen in bursts as it spasms from the trauma.  '
                ]
                let verbalText = [
                    `She howls in pain. "Break me!" she screams. `,
                    `"Split my little baby buttcheeks with your cock, big brother!" `,
                    `"Fuck my asshole, daddy! Fill my shitter with your cum!"`
                ]
                yourGirl.changeDignity(-2);
                if (yourGirl.av == 1) {
                    resultText += pickFrom(virginityText);
                    yourGirl.av = 0;
                    yourGirl.changeDignity(-150);
                    return resultText;
                } else {
                    resultText += pickFrom(regularText);
                }
                if (yourGirl.dignity < -100 && yourGirl.verbalSkill > 50) {
                    resultText += pickFrom(verbalText);
                }
                let oResult = player.addExcitation();
                if (oResult == 1) {
                    yourGirl.analCount++;
                    yourGirl.soil.anus++;
                    return pickFrom(orgasmText);
                } else {
                    return resultText
                }
            }
        },
        postEffect: function() {}
    },
    {
        title: 'pimp',
        type: 'default',
        display: 'Pimp Out',
        active: 0,
        requirements: function() {if ((yourGirl.fellatioCount >= 1 || player.money < 50 || yourGirl.vaginalCount >= 1) && yourGirl.atHome == 1 && yourGirl.isDressed() == 1) {return 1} else {return 0}},
        effect: function() {yourGirl.changeDignity(-1); return pickFrom(customers).effect()},
        postEffect: function() {}
    },
    {
        title: 'dressrandom',
        type: 'custom',
        display: 'Random outfit',
        active: 0,
        requirements: function() {if (yourGirl.atHome == 1) {return 1} else {return 0}},
        effect: function() {
            randomOutfit();
            return "She put on some clothes."},
        postEffect: function() {}
    },
    {
        title: 'defaultoutfit',
        type: 'custom',
        display: 'Favorite outfit',
        active: 0,
        requirements: function() {if (yourGirl.atHome == 1) {return 1} else {return 0}},
        effect: function() {
            wearFavoriteOutfit();
            return "She puts on your favorite outfit."},
        postEffect: function() {}
    },
    {
        title: 'clothing',
        type: 'shop',
        display: 'Tween Clothiers',
        active: 0,
        requirements: function() {if (player.money > 0) {return 1} else {return 0}},
        effect: function() {
            let itemcount = displayShop('clothes');
            if (itemcount == 0) {
                return "There is nothing left to buy at this time."
            } else {
                return "You go shopping."
            }
        },
        postEffect: function() {}
    },
    {
        title: 'foodshop',
        type: 'shop',
        display: 'Food Mart',
        active: 0,
        requirements: function() {if (player.money > 0 && yourGirl.atHome == 1) {return 1} else {return 0}},
        effect: function() {
            let itemcount = displayShop('food');
            if (itemcount == 0) {
                return "There is nothing left to buy at this time."
            } else {
                return "You go shopping."
            }
        },
        postEffect: function() {}
    },
    {
        title: 'sexshop',
        type: 'shop',
        display: 'Sex Shoppe',
        active: 0,
        requirements: function() {if (player.money > 0) {return 1} else {return 0}},
        effect: function() {
            let itemcount = displayShop('sex');
            if (itemcount == 0) {
                return "There is nothing left to buy at this time."
            } else {
                return "You go shopping."
            }
        },
        postEffect: function() {}
    },
    {
        title: 'petshop',
        type: 'shop',
        display: 'Pet Corp',
        active: 0,
        requirements: function() {if (player.money > 0 && (yourGirl.dignity < -50 || yourGirl.anima > 10)) {return 1} else {return 0}},
        effect: function() {
            let itemcount = displayShop('pet');
            if (itemcount == 0) {
                return "There is nothing left to buy at this time."
            } else {
                return "You go shopping."
            }
        },
        postEffect: function() {}
    },
    {
        title: 'miscshop',
        type: 'shop',
        display: 'Super Buy',
        active: 0,
        requirements: function() {if (player.money > 0) {return 1} else {return 0}},
        effect: function() {
            let itemcount = displayShop('misc');
            if (itemcount == 0) {
                return "There is nothing left to buy at this time."
            } else {
                return "You go shopping."
            }
        },
        postEffect: function() {}
    },
    {
        title: 'wardrobe',
        type: 'custom',
        display: 'Change clothes',
        active: 0,
        requirements: function() {if (wardrobe.length > 0 && yourGirl.atHome == 1) {return 1} else {return 0}},
        effect: function() {
            let itemcount = showWardrobe();
            if (itemcount == 0) {
                return "Your wardrobe is empty."
            } else {
                return "You look for a new outfit."
            }
        },
        postEffect: function() {}
    },
    {
        title: 'inventory',
        type: 'custom',
        display: 'View belongings',
        active: 0,
        requirements: function() {if (inventory.length > 0 && yourGirl.atHome == 1) {return 1} else {return 0}},
        effect: function() {
            let itemcount = showInventory();
            if (itemcount == 0) {
                return "Your inventory is empty."
            } else {
                return "You look through your belongings."
            }
        },
        postEffect: function() {}
    },
    {
        title: 'bathe',
        type: 'custom',
        display: 'Bathe her',
        active: 0,
        requirements: function() {if (yourGirl.atHome == 1 && yourGirl.isNude() == 1) {return 1} else {return 0}},
        effect: function() {
            yourGirl.hygiene = 3;
            yourGirl.changeDignity(5);
            for (var property in yourGirl.soil) {
                yourGirl.soil[property] = 0;
            }
            yourGirl.lubrication = 0;
            yourGirl.lubeSource = "";
            player.addArousal('nudity');
            return "You run the bath and lead her in. You carefully clean her body from head to toe, leaving no crack unattended."},
        postEffect: function() {}
    }
]

function buyClothes(id) {
    if (player.money >= apparelList[id].cost) {
        player.money -= apparelList[id].cost;
        wardrobe.push(id);
        $("#shopstatus").html(`You bought ${apparelList[id].display}. `)
        displayShop(apparelList[id].shopCategory);
    }
    saveGame();
}

function buyItem(id) {
    if (player.money >= shopItems[id].cost) {
        player.money -= shopItems[id].cost;
        if (shopItems[id].itemType == 'owned') {
            inventory.push(id);
            $("#shopstatus").html(`You bought ${shopItems[id].display}. `)
        } else if (shopItems[id].itemType == 'consumed') {
            $("#shopstatus").html(shopItems[id].effect());
        }
        displayShop(shopItems[id].itemType);
    } else {
        GAMEVAR.statusQueue.push(['shopstatus','Not enough money.'])
    }
    saveGame();
}

/* function displayShop(type) {
    $("#shop").html("");
    let itemcount = 0;
    if (type == 'clothes') {
        for (let i=0;i<apparelList.length;i++) {
            if (wardrobe.indexOf(i) == -1 && apparelList[i].traits.indexOf('pet') == -1) {
                let shopitem = `<a href="javascript:;" class="shopitem" data-itemId="${i}" onclick="buyClothes(${i})">${apparelList[i].display} ($${apparelList[i].cost})</a><br>`;
                itemcount++
                $("#shop").append(shopitem);
            }
        }
    } else {
        for (let i=0;i<shopItems.length;i++) {
            if (inventory.indexOf(i) == -1) {
                if (shopItems[i].shopCategory == type) {
                    let shopitem = `<a href="javascript:;" class="shopitem" data-itemId="${i}" onclick="buyItem(${i})">${shopItems[i].display} ($${shopItems[i].cost})</a><br>`;
                    itemcount++
                    $("#shop").append(shopitem);
                }
            }
        }
    }
    return itemcount
} */

function displayShop(type) {
    $("#shop").html("");
    let itemcount = 0;
    for (let i=0;i<apparelList.length;i++) {
        if (wardrobe.indexOf(i) == -1 && apparelList[i].shopCategory == type) {
            let shopitem = `<a href="javascript:;" class="shopitem" data-itemId="${i}" onclick="buyClothes(${i})">${apparelList[i].display} ($${apparelList[i].cost})</a><br>`;
            itemcount++
            $("#shop").append(shopitem);
        }
    }
    for (let i=0;i<shopItems.length;i++) {
        if (inventory.indexOf(i) == -1 && shopItems[i].shopCategory == type) {
            let shopitem = `<a href="javascript:;" class="shopitem" data-itemId="${i}" onclick="buyItem(${i})">${shopItems[i].display} ($${shopItems[i].cost})</a><br>`;
            itemcount++
            $("#shop").append(shopitem);
        }
    }
    return itemcount
}

function showWardrobe() {
    $("#inventorysection").html("");
    let itemcount = 0;
    for (let i=0;i<wardrobe.length;i++) {
        let prefix = "";
        if (yourGirl.equippedClothes.indexOf(i) > -1) {prefix = "remove "}
        let wardrobeitem = `<a href="javascript:;" class="wardrobeitem" data-wardrobeId="${i}" onclick="changeClothes(${i})">${prefix}${apparelList[wardrobe[i]].display}</a><br>`;
        itemcount++
        $("#inventorysection").append(wardrobeitem);
    }
    let favelink = `<a href="javascript:;" class="mt-3" onclick="saveFavorite()">Save outfit as favorite</a> `;
    $("#inventorysection").append(favelink)
    return itemcount;
}

function showInventory() {
    $("#inventorysection").html("");
    let itemcount = 0;
    for (let i=0;i<inventory.length;i++) {
        let item = `<a href="javascript:;" class="wardrobeitem" data-wardrobeId="${i}" onclick="useItem(${i})">${shopItems[inventory[i]].display}</a><br>`;
        itemcount++
        $("#inventorysection").append(item);
    }
    return itemcount;
}

function useItem(inventoryindex) {
    GAMEVAR.statusQueue.push(['customstatus',shopItems[inventory[inventoryindex]].effect()])
}

function wearFavoriteOutfit() {
    yourGirl.equippedClothes = GAMEVAR.favoriteOutfit;
}

function saveFavorite() {
    GAMEVAR.favoriteOutfit = yourGirl.equippedClothes;
    $("#customstatus").html("Outfit saved");
}

function closeExpansions() {
    $("#inventorysection").html("");
    $("#shop").html("");
}

function changeClothes(wardrobeindex) {
    if (yourGirl.equippedClothes.indexOf(wardrobeindex) > -1) {
        yourGirl.removeApparel(yourGirl.equippedClothes.indexOf(wardrobeindex));
    } else {
        yourGirl.wearApparel(wardrobeindex);
    }
    saveGame();
    showWardrobe();
}

function randomOutfit() {
    yourGirl.equippedClothes = [];
    let underwear = [];
    let top = [];
    let bottom = [];
    if (yourGirl.dignity > 0) {
        for (let i=0;i<wardrobe.length;i++) {
            if (apparelList[wardrobe[i]].location.isUnderwear == true) {
                underwear.push(i);
            }
        }
        yourGirl.wearApparel(pickFrom(underwear));
    }
    for (let i=0;i<wardrobe.length;i++) {
        if (apparelList[wardrobe[i]].location.isTop == true) {
            top.push(i);
        }
    }
    for (let i=0;i<wardrobe.length;i++) {
        if (apparelList[wardrobe[i]].location.isBottom == true) {
            bottom.push(i);
        }
    }
    do {
        yourGirl.wearApparel(pickFrom(top));
        if (yourGirl.isDressed() == 0) {
            yourGirl.wearApparel(pickFrom(bottom));
        }
    } while (yourGirl.isDressed() == 0);
}

function closeShop() {
    $("#shop").html("");
}

function showPage(page) {
    $(".gamepage").hide();
    if (page == 'customize') {
        renderPage('customize');
        $("#customizepage").show();
    }
    if (page == 'main') {
        renderPage('main');
        $("#mainpage").show();
    }
    if (page == 'stats') {
        renderPage('stats');
        $("#statspage").show();
    }
}

function renderPage(page) {
    if (page == 'main') {
        $("#mainpage").html(mainPage());
        for (let i=0;i<gameActions.length;i++) {
            if (gameActions[i].active==1) {addAction(i)}
        }
    }
    if (page == 'stats') {
        $("#statspage").html(statsPage());
    }
    if (page == 'customize') {
        $("#customizepage").html(customizePage());
        for (let i=0;i<gameActions.length;i++) {
            if (gameActions[i].active==1) {addAction(i)}
        }
    }
}

$(document).ready(gameStart());